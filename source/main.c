/*****************************************************************************\
*  libWiiGP Example                                                           *
*  Copyright (c) 2009         Alex Marshall (SquidMan) <SquidMan72@gmail.com> *
*  Copyright (c) 2009           Thomas Daede (TD-Linux) <bztdlinux@gmail.com> *
*  Copyrighted under the GNU GPLv2                                            *
\*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <gccore.h>
#include <ogc/irq.h>
#include <wiiuse/wpad.h>

#include "wiigp.h"

void *xfb = NULL;
static GXRModeObj *rmode = NULL;
void *efb;

int initialize()
{
	VIDEO_Init();
	WPAD_Init();
	sleep(1);
	rmode = VIDEO_GetPreferredMode(NULL);
	if(rmode == NULL)
		return -1;
	xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));
	if(xfb == NULL)
		return -1;
	console_init(xfb,20,20,rmode->fbWidth,rmode->xfbHeight,rmode->fbWidth*VI_DISPLAY_PIX_SZ);
	VIDEO_Configure(rmode);
	VIDEO_SetNextFramebuffer(xfb);
	VIDEO_SetBlack(FALSE);
	VIDEO_Flush();
	VIDEO_WaitVSync();
	if(rmode->viTVMode&VI_NON_INTERLACE)
		VIDEO_WaitVSync();
	sleep(2);	/* A little sleep just to make sure everything init'ed all nice. */
	return 0;
}

int main(int argc, char **argv)
{
	if(initialize() < 0)
		exit(1);
	printf("\x1b[2;0HHello World!\n");
	
	initialize_wiigp();
	efb = (void*)WIIGP_EFB;
	while(1) {
		WPAD_ScanPads();
		u32 pressed = WPAD_ButtonsDown(0);
		if(pressed & WPAD_BUTTON_HOME)
			break;

		if(pressed & WPAD_BUTTON_1) {
//			wiigp_viewport(0, 0, WIIGP_WIDTH, WIIGP_HEIGHT, 0, 1);
			wiigp_set_copy_box(0, 0, WIIGP_WIDTH, WIIGP_HEIGHT);
//			wiigp_clipping(0, 0, WIIGP_WIDTH, WIIGP_HEIGHT);
//			wiigp_field_mode(rmode->field_rendering, (rmode->viHeight == (2 * rmode->xfbHeight)) ? 1 : 0);
			wiigp_set_stride(WIIGP_WIDTH);
			wiigp_set_xfb_address(xfb);
			wiigp_blit_screen(1);
		}
		
		if(pressed & WPAD_BUTTON_A) {
			printf("CP Pipe Status: %04x\n", CP_PIPE_STATUS);
			printf("CP Control:     %04x\n", CP_CONTROL);
			printf("CP Pipe Start:  %08x\n", GX32SWAP(CP_PIPE_START));
			printf("CP Pipe End:    %08x\n", GX32SWAP(CP_PIPE_END));
			printf("CP Pipe WStart: %08x\n", GX32SWAP(CP_PIPE_WSTART));
			printf("CP Pipe WEnd:   %08x\n", GX32SWAP(CP_PIPE_WEND));
			printf("CP Pipe Dist:   %08x\n", GX32SWAP(CP_PIPE_DIST));
			printf("CP Pipe WP:     %08x\n", GX32SWAP(CP_PIPE_WRITE_PTR));
			printf("CP Pipe RP:     %08x\n", GX32SWAP(CP_PIPE_READ_PTR));
			printf("PI Pipe WP:     %08x\n", PI_PIPE_WRITE_PTR);
		}

		if(pressed & WPAD_BUTTON_B) {
			wiigp_viewport(0, 0, WIIGP_WIDTH, WIIGP_HEIGHT, 0, 1);
			printf("Writing EFB...\n");
			memset(efb, 0xAA, 640 * 480 * 4);
			printf("EFB write complete.\n");
			printf("Blitting screen...\n");
			usleep(100);
/*			wiigp_viewport(0, 0, WIIGP_WIDTH, WIIGP_HEIGHT, 0, 1);
			printf("Set viewport... ");
			usleep(100);*/
			wiigp_set_copy_box(0, 0, WIIGP_WIDTH, WIIGP_HEIGHT);
			printf("Set copy box... ");
			usleep(100);
/*			wiigp_clipping(0, 0, WIIGP_WIDTH, WIIGP_HEIGHT);
			printf("Set clipping... ");
			usleep(100);
			wiigp_field_mode(rmode->field_rendering, (rmode->viHeight == (2 * rmode->xfbHeight)) ? 1 : 0);
			printf("Set field mode... ");
			usleep(100);*/
			wiigp_set_stride(WIIGP_WIDTH);
			printf("Set stride... ");
			usleep(100);
			wiigp_set_xfb_address(xfb);
			printf("Set XFB... ");
			usleep(100);
			wiigp_blit_screen(1);
			printf("Blitting complete.\n");
		}
		// Wait for the next frame
		VIDEO_Flush();
		VIDEO_WaitVSync();
	}
	shutdown_wiigp();
	return 0;
}
