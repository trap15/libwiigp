/*****************************************************************************
 *  libWiiGP                                                                 *
 *  Copyright (c) 2009       Alex Marshall (SquidMan) <SquidMan72@gmail.com> *
 *  Copyright (c) 2009         Thomas Daede (TD-Linux) <bztdlinux@gmail.com> *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <gccore.h>
#include <ogcsys.h>
#include <ogc/irq.h>
#include <ogc/machine/processor.h>
#include <math.h>
#include <wiiuse/wpad.h>

#include "wiigp.h"

#define GP_F32_TO_U32(x)			((u32)(((f32)(x)) * 16777215.0))
#define SOMEOFFSET				(342)
#define BP_DRAW_SCREEN				(0x4000)
#define BP_CLEAR_AFTER				(0x0800)
#define BP_DONE_DRAWING				(0x02)
#define PIPE_BP_COMMAND				(0x61)
#define PIPE_CP_COMMAND				(0x08)
#define PIPE_XF_COMMAND				(0x10)
#define PE_FINISH_ACK				(1 << 3)
#define PE_TOKEN_ACK				(1 << 2)
#define PE_FINISH_ENABLE			(1 << 1)
#define PE_TOKEN_ENABLE				(1 << 0)

#define CP_BP_INT				(1 << 4)
#define CP_UNDERFLOW				(1 << 1)
#define CP_OVERFLOW				(1 << 0)

#define CP_BP_EN				(1 << 5)
#define CP_UNDERFLOW_IRQ_EN			(1 << 3)
#define CP_OVERFLOW_IRQ_EN			(1 << 2)

#define BYTEMASK(x)				((1 << x) - 1)

#define ResetPipe()				mtwpar(MEM_VIRTUAL_TO_PHYSICAL(CP_PIPE))
#define EnablePipe()				mthid2(mfhid2() |  (1 << 30))
#define DisablePipe()				mthid2(mfhid2() & ~(1 << 30))
#define wiigp_link_fifo(x)			CP_CONTROL |= (CP_BP_INT * (x))
#define wiigp_reset_fifo_interrupts(x, y)	CP_CLEAR   |= (CP_UNDERFLOW        * (x)) | (CP_OVERFLOW        * x);
#define wiigp_enable_fifo_interrupts(x, y)	CP_CONTROL |= (CP_UNDERFLOW_IRQ_EN * (x)) | (CP_OVERFLOW_IRQ_EN * x);
#define wiigp_disable_fifo_interrupts(x, y)	wiigp_enable_fifo_interrupts(x, y)

int waitfinish = 0;
u8* gp_pipe = NULL;
extern void __MaskIrq(u32);
extern void __UnmaskIrq(u32);

static void _pipe_putu8(u8 byte)
{
	*(vu8*)CP_PIPE = byte;
}

static void _pipe_putu32(u32 word)
{
	*(vu32*)CP_PIPE = word;
}

static void _pipe_putf32(f32 f)
{
	*(vf32*)CP_PIPE = f;
}

static void _bp_write(u8 reg, u32 params)
{
	_pipe_putu8(PIPE_BP_COMMAND);
	params &= BYTEMASK(24);
	_pipe_putu32((reg << 24) | params);
}

static void _cp_write(u8 reg, u32 params)
{
	_pipe_putu8(PIPE_CP_COMMAND);
	_pipe_putu8(reg);
	_pipe_putu32(params);
}

static void _xf_write(u16 reg, u32 params)
{
	_pipe_putu8(PIPE_XF_COMMAND);
	_pipe_putu32(reg);
	_pipe_putu32(params);
}

static void _xf_write_multi(u16 sreg, u16 count)
{
	_pipe_putu8(PIPE_XF_COMMAND);
	sreg &= BYTEMASK(4);
	count--;
	count &= BYTEMASK(4);
	_pipe_putu32(count | sreg);
}

static void CP_Underflow_Handler()
{
//	printf("CP UNDERFLOW OCCURRED!\n");
	wiigp_reset_fifo_interrupts(WIIGP_TRUE, WIIGP_FALSE);
}

static void CP_Overflow_Handler()
{
//	printf("CP OVERFLOW OCCURRED!\n");
	wiigp_reset_fifo_interrupts(WIIGP_FALSE, WIIGP_TRUE);
}

static void CP_BP_Handler()
{
	printf("BP INTERRUPT OCCURRED!\n");
	CP_CONTROL &= ~CP_BP_EN;
}

static void CP_Int_Handler(u32 irq, void *unused)
{
	u16 pipestatus  = CP_PIPE_STATUS;
	u16 pipecontrol = CP_CONTROL;
	if((pipecontrol & CP_UNDERFLOW_IRQ_EN) && (pipestatus & CP_UNDERFLOW))
		CP_Underflow_Handler();
	if((pipecontrol & CP_OVERFLOW_IRQ_EN) && (pipestatus & CP_OVERFLOW))
		CP_Overflow_Handler();
	/* Is the enable really on Status, and the interrupt on Control? GX really fails it. */
	/* It's also totally different from the rest of the GX interrupts :/ */
	if((pipecontrol & CP_BP_EN) && (pipestatus & CP_BP_INT)) {
		CP_BP_Handler();
	}

}

static void PE_Token_Int_Handler(u32 irq, void *unused)
{
	printf("PE TOKEN INTERRUPT OCCURRED!\n");
	u16 token = PE_TOKEN;
	PE_STATUS |= PE_TOKEN_ACK;
}

static void PE_Finish_Int_Handler(u32 irq, void *unused)
{
	printf("PE FINISH INTERRUPT OCCURRED!\n");
	PE_STATUS |= PE_FINISH_ACK;
	waitfinish = 1;
}


static void InitFIFOInterrupts()
{
	IRQ_Request(IRQ_PI_CP, CP_Int_Handler, NULL);
	__UnmaskIrq(IRQMASK(IRQ_PI_CP));
}

static void InitPEInterrupts()
{
	IRQ_Request(IRQ_PI_PETOKEN, PE_Token_Int_Handler, NULL);
	__UnmaskIrq(IRQMASK(IRQ_PI_PETOKEN));
	IRQ_Request(IRQ_PI_PEFINISH, PE_Finish_Int_Handler, NULL);
	__UnmaskIrq(IRQMASK(IRQ_PI_PEFINISH));
	
	PE_STATUS |= PE_FINISH_ENABLE;
	PE_STATUS |= PE_TOKEN_ENABLE;
	PE_STATUS |= PE_FINISH_ACK;
	PE_STATUS |= PE_TOKEN_ACK;
}

int initialize_wiigp()
{
	gp_pipe = (u8*)memalign(32, DEFAULT_PIPE_SIZE);
	if(gp_pipe == NULL)
		return -1;
	InitFIFOInterrupts();

	PI_PIPE_BASE_START = MEM_VIRTUAL_TO_PHYSICAL(gp_pipe);
	PI_PIPE_BASE_END   = MEM_VIRTUAL_TO_PHYSICAL(gp_pipe + DEFAULT_PIPE_SIZE);
	PI_PIPE_WRITE_PTR  = PI_PIPE_BASE_START;
	
	wiigp_disable_fifo_interrupts(WIIGP_DISABLE, WIIGP_DISABLE);
	ppcsync();

	/********************************************************************/
	/**  Disable reads                                                  */
	/**/ CP_CONTROL &= ~(1 << 0);
	/**  Disable interrupts                                             */
	/**/ wiigp_disable_fifo_interrupts(WIIGP_DISABLE, WIIGP_DISABLE);
	/********************************************************************/
	
	CP_PIPE_START     = GX32SWAP(MEM_VIRTUAL_TO_PHYSICAL(gp_pipe));
	CP_PIPE_END       = GX32SWAP(MEM_VIRTUAL_TO_PHYSICAL(gp_pipe + DEFAULT_PIPE_SIZE));
	CP_PIPE_DIST      = 0;
	CP_PIPE_WSTART    = DEFAULT_PIPE_SIZE - (16 * 1024);
	CP_PIPE_WEND      = DEFAULT_PIPE_SIZE >> 1;
	CP_PIPE_WRITE_PTR = CP_PIPE_START;
	CP_PIPE_READ_PTR  = CP_PIPE_WRITE_PTR;
	ppcsync();

	/******************************************************************/
	/**  Enable interrupts                                            */
	/**/ wiigp_enable_fifo_interrupts(WIIGP_ENABLE, WIIGP_DISABLE);
	/**  Let's also link the Pipe.                                    */
	/**/ wiigp_link_fifo(WIIGP_ENABLE);
	/**  Let's reset the interrupts while we're at it!                */
	/**/ wiigp_reset_fifo_interrupts(WIIGP_TRUE, WIIGP_TRUE);
	/**  Enable reads                                                 */
	/**/ CP_CONTROL |= (1 << 0);
	/******************************************************************/
	ppcsync();
	
	InitPEInterrupts();
	
	/*****************************************************/
	/**  Move the Pipe's Physical address into the WPAR. */
	/**/ ResetPipe();
	/**  Enable the Pipe.                                */
	/**/ EnablePipe();
	/*****************************************************/
	
	/***********************************************************/
	/**  Some crazy clock divider crazyness!                   */
	/**/ u32 divis = 500;
	/**/ u32 res = (u32)(TB_BUS_CLOCK / divis);
	/**/ _bp_write(BP_REG_CLOCK_1, (res >> 11) | (1 << 10));
	/**  Some more crazy clock divider crazyness!              */
	/**/ divis = 4224;
	/**/ res = (u32)(res / divis);
	/**/ _bp_write(BP_REG_CLOCK_0, res | (1 << 9));
	/***********************************************************/	
	int i;
	for(i = 0; i < 8; i++)
		_cp_write(CP_REG_VERT_ATTR(1, i), 0x80000000);
	
	/********************************************/
	/**  Gotta ask shagkur what these do.       */
	/**/ _xf_write(XF_REG_ERROR, 0x3F);
	/**/ _xf_write(XF_REG_DUAL_TEX_TRANS, 1);
	/**/ _bp_write(BP_REG_VFILTER(5), 0x0F);
	/********************************************/

	/********************************************/
	/**  Selects Performance register 0         */
	/**/ CP_PERF_SELECT = 0;
	/********************************************/

	/********************************************/
	/**  Gotta ask shagkur what this does.      */
	/**/ _cp_write(CP_REG_UNKNOWN,     0);
	/********************************************/

	/********************************************/
	/**  Clears the Performance register 0      */
	/**/ _xf_write(XF_REG_PERF(0),     0);
	/********************************************/

	/********************************************/
	/**  Gotta ask shagkur what these do.       */
	/**/ _bp_write(BP_REG_SU_COUNTER,  0);
	/**/ _bp_write(BP_REG_RAS_COUNTER, 0);
	/**/ _bp_write(BP_REG_GP_METRIC,   0);
	/********************************************/	

	/*****************************************/	
	/**  Clear the Indirect Mask.            */
	/**/ _bp_write(BP_IND_IMASK,       0);
	/*****************************************/
	
	/******************************************************/
	/** I have no clue what this really does.             */
	/** Probably important though...                      */
	/**- - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/** Set Texture Image 0-3, GXTexMapID 0 - 3           */
	/** Tmem: 0x00000000, Cache Width 32 kb               */
	/** Cache Height 32 kb, Image Type Cached             */
	/**/ _bp_write(BP_REG_TX_IMAGE(1, 0, 0), 0x0D8000);
	/**/ _bp_write(BP_REG_TX_IMAGE(1, 1, 0), 0x0D8800);
	/**/ _bp_write(BP_REG_TX_IMAGE(1, 2, 0), 0x0D9000);
	/**/ _bp_write(BP_REG_TX_IMAGE(1, 3, 0), 0x0D9800);
	/**/ _bp_write(BP_REG_TX_IMAGE(2, 0, 0), 0x0DC000);
	/**/ _bp_write(BP_REG_TX_IMAGE(2, 1, 0), 0x0DC800);
	/**/ _bp_write(BP_REG_TX_IMAGE(2, 2, 0), 0x0DD000);
	/**/ _bp_write(BP_REG_TX_IMAGE(2, 3, 0), 0x0DD800);
	/**- - - - - - - - - - - - - - - - - - - - - - - - - -*/
	/** Set Texture Image 0-3, GXTexMapID 4 - 7           */
	/** Tmem: 0x00010000, Cache Width 32 kb               */
	/** Cache Height 32 kb, Image Type Cached             */
	/**/ _bp_write(BP_REG_TX_IMAGE(1, 0, 1), 0x0DA000);
	/**/ _bp_write(BP_REG_TX_IMAGE(1, 1, 1), 0x0DA800);
	/**/ _bp_write(BP_REG_TX_IMAGE(1, 2, 1), 0x0DB000);
	/**/ _bp_write(BP_REG_TX_IMAGE(1, 3, 1), 0x0DB800);
	/**/ _bp_write(BP_REG_TX_IMAGE(2, 0, 1), 0x0DC400);
	/**/ _bp_write(BP_REG_TX_IMAGE(2, 1, 1), 0x0DCC00);
	/**/ _bp_write(BP_REG_TX_IMAGE(2, 2, 1), 0x0DD400);
	/**/ _bp_write(BP_REG_TX_IMAGE(2, 3, 1), 0x0DDC00);
	/******************************************************/
	return 0;
}

void shutdown_wiigp()
{
	/********************************************************************/
	/**  Disable reads                                                  */
	/**/ CP_CONTROL &= ~(1 << 0);
	/**  Disable interrupts                                             */
	/**/ wiigp_disable_fifo_interrupts(WIIGP_DISABLE, WIIGP_DISABLE);
	/********************************************************************/
	ppcsync();
	wiigp_link_fifo(WIIGP_DISABLE);
	ppcsync();	
	free(gp_pipe);
}

void wiigp_set_copy_box(u32 x, u32 y, u32 width, u32 height)
{
	x &= BYTEMASK(10);
	y &= BYTEMASK(14);
	width--;
	height--;
	width &= BYTEMASK(10);
	height &= BYTEMASK(14);
	/**********************************************************/
	/**  Set top left corner                                  */
	/**/ _bp_write(BP_REG_BOXCOORD, (y << 10) | x);
	/**  Set size                                             */
	/**/ _bp_write(BP_REG_BOXSIZE, (height << 10) | width);
	/**********************************************************/
}

void wiigp_set_xfb_address(void* xfbaddr)
{
	_bp_write(BP_REG_XFBADDR, MEM_VIRTUAL_TO_PHYSICAL(xfbaddr) >> 5);
}

void wiigp_set_stride(u32 stride)
{
	_bp_write(BP_REG_STRIDE, stride);
}

void wiigp_done_drawing()
{
	_bp_write(BP_REG_DRAW_DONE, BP_DONE_DRAWING);
	pipe_flush();
	waitfinish = 0;
	while(!waitfinish);
}

void wiigp_blit_screen(u8 clear)
{
	wiigp_done_drawing();
	clear &= 1;
		
	_bp_write(BP_REG_COPY_CONTROL, BP_DRAW_SCREEN | (clear * BP_CLEAR_AFTER));
}

void wiigp_set_copy_clear_color(u8 r, u8 g, u8 b, u8 a, float depth)
{
	_bp_write(BP_REG_CLEAR_COLOR_H, (a << 8) | r);
	_bp_write(BP_REG_CLEAR_COLOR_L, (g << 8) | b);
	if((depth <= 1.0) && (depth >= 0.0))
		_bp_write(BP_REG_CLEAR_DEPTH, (u32)(depth * 16777215.0));
}

void wiigp_viewport(float x, float y, float width, float height, float near_z, float far_z)
{
	float w,  h;
	float rx, ry;
	u32   f, dz;

	/***********************************************/
	/**  Divide the width and height by two (why?) */
	/**/ width	*= 0.5;
	/**/ height	*= 0.5;
	/**  These are actually the width and height,  */
	/**  not the top left coordinates.             */
	/**/ w =  width;
	/**/ h = -height;
	/**  Alright, what is SOMEOFFSET :|            */
	/**/ rx = x + width  + SOMEOFFSET;
	/**/ ry = y + height + SOMEOFFSET;
	/**  Calculate the Z distance.                 */
	/**/ f  =     GP_F32_TO_U32( far_z);
	/**/ dz = f - GP_F32_TO_U32(near_z);
	/**  Now we can write out all the information  */
	/***********************************************/
	/**  Write 6 chunks to the XF Viewport Reg.    */
	/**/ _xf_write_multi(XF_REG_VIEWPORTS, 6);
	/**/ _pipe_putf32(w);  /* Write the Width      */
	/**/ _pipe_putf32(h);  /* Write the Height     */
	/**/ _pipe_putu32(dz); /* Write the Depth      */
	/**/ _pipe_putf32(rx); /* Write the X location */
	/**/ _pipe_putf32(ry); /* Write the Y location */
	/**/ _pipe_putu32(f);  /* Write the Z location */
	/***********************************************/
}

void wiigp_clipping(u32 x, u32 y, u32 width, u32 height)
{
	u32 tl_x, tl_y, br_x, br_y;
	tl_x = x + SOMEOFFSET;
	tl_y = y + SOMEOFFSET;
	br_x = x + SOMEOFFSET + (width  - 1);
	br_y = y + SOMEOFFSET + (height - 1);

	tl_x &= BYTEMASK(12);
	tl_y &= BYTEMASK(12);
	br_x &= BYTEMASK(12);
	br_y &= BYTEMASK(12);
	
	_bp_write(BP_REG_SCISSOR_TL, (tl_x << 12) | tl_y);
	_bp_write(BP_REG_SCISSOR_BR, (br_x << 12) | br_y);
}

void wiigp_field_mode(u8 field_mode,u8 half_aspect_ratio)
{
	half_aspect_ratio &= BYTEMASK(2);
	/**************************************************************/
	/**  I have literally no clue what this does.                 */
	/**/ _bp_write(BP_REG_LINE_WIDTH, half_aspect_ratio << 22);
	/**/ _bp_write(BP_REG_FIELD_MODE, field_mode);
	/**************************************************************/
}

void wiigp_set_copy_filter(u32 a, u32 b, u32 c, u32 d)
{
	_bp_write(BP_REG_FILTER(0), a);
	_bp_write(BP_REG_FILTER(1), b);
	_bp_write(BP_REG_FILTER(2), c);
	_bp_write(BP_REG_FILTER(3), d);
}

void wiigp_no_antialias()
{
	/**********************************************************************/
	/**  Apparently these values set a clean copy filter (no effects)     */
	/**/ wiigp_set_copy_filter(0x666666, 0x666666, 0x666666, 0x666666);
	/**********************************************************************/
}

void wiigp_set_vertical_filter(u8 a, u8 b, u8 c, u8 d, u8 e, u8 f, u8 g)
{
	a &= BYTEMASK(6);
	b &= BYTEMASK(6);
	c &= BYTEMASK(6);
	d &= BYTEMASK(6);
	e &= BYTEMASK(6);
	f &= BYTEMASK(6);
	g &= BYTEMASK(6);
	_bp_write(BP_REG_VFILTER(0), (d << 18) | (c << 12) | (b << 6) | a);
	_bp_write(BP_REG_VFILTER(1), (g << 12) | (f <<  6) |  e);
}

void wiigp_normal_vertical_filter()
{
	/****************************************************************************/
	/**  Apparently these values set a clean vertical filter (no effects)       */
	/**/ wiigp_set_vertical_filter(0x00, 0x00, 0x15, 0x16, 0x15, 0x00, 0x00);
	/****************************************************************************/
}

void pipe_flush()
{
	_pipe_putu32(0);
	_pipe_putu32(0);
	_pipe_putu32(0);
	_pipe_putu32(0);
	_pipe_putu32(0);
	_pipe_putu32(0);
	_pipe_putu32(0);
	_pipe_putu32(0);

	ppcsync();
}
